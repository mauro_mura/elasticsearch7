# Elasticsearch up and running
Setup a dev server with Elasticsearch and Kibana 7.x 

## Tools required
- VirtualBox 6.1.x
- vagrant 2.2.x

## Installation

```
$ git clone git@bitbucket.org:mauro_mura/elasticsearch7.git ./elastic7
$ cd elastic7 && vagrant up
```
## Test
```
$ curl localhost:9200
{
  "name" : "node-1",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "Q-l3cjNBRP-fX3MO8OEIgw",
  "version" : {
    "number" : "7.12.0",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "78722783c38caa25a70982b5b042074cde5d3b3a",
    "build_date" : "2021-03-18T06:17:15.410153305Z",
    "build_snapshot" : false,
    "lucene_version" : "8.8.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```